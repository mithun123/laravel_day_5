<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//https://gitlab.com/mdahosanhabib/technobd_laravel_batch_3
//git clone https://gitlab.com/mdahosanhabib/technobd_laravel_batch_3.git
//copy .env.example to .env
//composer update
//php artisan config:clear
//php artisan config:cache
//php artisan key:generate

//1.migration
//2.seeding
//3.factory
//4.faker
//5.working with tinker

Route::get('/', 'WelcomeController@home');

Route::get('/students', 'WelcomeController@students');

Route::prefix('admin')->group(function () {

    Route::get('/', 'DashboardController@home');

    Route::resource('/categories', 'CategoriesController');

//    Route::prefix('categories')->group(function () {

//        Route::get('/', 'CategoriesController@index')->name('categories.index');
//        Route::get('/create', 'CategoriesController@create')->name('categories.create');
//        Route::post('/', 'CategoriesController@store')->name('categories.store');
//        Route::get('/{id}', 'CategoriesController@show')->name('categories.show');
//        Route::get('/{id}/edit', 'CategoriesController@edit')->name('categories.edit');
//        Route::put('/{id}', 'CategoriesController@update')->name('categories.update');
//        Route::delete('/{id}', 'CategoriesController@destroy')->name('categories.destroy');
//    });

});



