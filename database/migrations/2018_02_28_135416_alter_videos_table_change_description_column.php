<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterVideosTableChangeDescriptionColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('videos', function (Blueprint $table) {
            $table->string('description')->nullable(false)->change();
        });
        Schema::table('videos', function (Blueprint $table) {
            $table->renameColumn('description', 'details');
            // $table->string('details')->nullable(false)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('videos', function (Blueprint $table) {
            $table->text('description')->nullable()->change();
            //  $table->renameColumn('details', 'description');
        });
    }
}
